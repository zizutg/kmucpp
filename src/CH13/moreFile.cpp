// This program writes information to a file, closes the file,
// then reopens it and appends more information.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

// Function prototype
void showContents(fstream &);
void showChar(fstream &);
void showLine(fstream &);
void copyFile(fstream &);

int mainMRF() {
	fstream dataFile; // file object

	// Open a file to write to, and write to it.
	dataFile.open("demofile.txt", ios::out);
	dataFile << "Jones\n";
	dataFile << "Smith\n";

	// Close the file.
	dataFile.close();

	// Open the same file in append mode, and write to it.
	dataFile.open("demofile.txt", ios::out | ios::app);
	dataFile << "Will Smith\n";
	dataFile << "David Guetta\n";

	// Close the file.
	dataFile.close();

	//show the content using << operator
	dataFile.open("demofile.txt", ios::in);
	showContents(dataFile);
	dataFile.close();

	//show each character of the content
	dataFile.open("demofile.txt", ios::in);
	showChar(dataFile);
	dataFile.close();

	//show each line of the the content, not breaking at space like <<
	dataFile.open("demofile.txt", ios::in);
	showLine(dataFile);
	dataFile.close();

	//show each line of the the content, not breaking at space like <<
	dataFile.open("demofile.txt", ios::in);
	copyFile(dataFile);
	dataFile.close();

	return 0;
}

void showContents(fstream &file) {
	string str;

	file >> str;
	while (!file.fail()) {
		cout << str << endl;
		file >> str;
	}
}

void showChar(fstream &file) {
	char ch;

	file.get(ch);
	while (!file.fail()) {
		cout << ch << endl;
		file.get(ch);
	}
}

void showLine(fstream &file) {
	char input[81];

	if (!file) {
		cout << "File open Error" << endl;
	}

	file.getline(input, 81);

	while (!file.fail()) {
		cout << input << endl;
		file.getline(input, 81);
	}
}

void copyFile(fstream &file) {
	fstream outFile; // file object
	char ch;
	// Open a file to write to, and write to it.
	outFile.open("demoOutfile.txt", ios::out);

	file.get(ch);
	while (!file.fail()) {
		outFile.put(ch);
		file.get(ch);
	}

	outFile.close();

}

