// Lab 3 percentage.cpp
// This program will determine the percentage 
// of answers a student got correct on a test.
// PUT YOUR NAME HERE.

// INCLUDE THE FILE NEEDED TO DO I/O
#include <iostream>
// INCLUDE THE FILE NEEDED TO FORMAT OUTPUT
#include <string>
// INCLUDE THE FILE NEEDED TO USE STRINGS
#include <iomanip>
using namespace std;

int mainPR()
{
   string name;
   double numQuestions,
       numCorrect;
   double percentage;
   
   // Get student's test data
   cout << "Enter student's first and last name: ";
   getline(cin, name);
   // WRITE A STATEMENT TO READ THE WHOLE NAME INTO THE name VARIABLE.
   
   cout << "Number of questions on the test: ";
   cin  >> numQuestions;
   cout << "Number of answers the student got correct: ";
   cin  >> numCorrect;
   
   // Compute and display the student's % correct
   // WRITE A STATEMENT TO COMPUTE THE % AND ASSIGN THE RESULT TO percentage.
   percentage = (numCorrect/numQuestions) * 100;
   // WRITE STATEMENTS TO DISPLAY THE STUDENT'S NAME AND THEIR TEST 
   // PERCENTAGE WITH ONE DECIMAL POINT. 
   cout << name <<"  "<< percentage;

   
   return 0;
}
