/*
 * TestFile.cpp
 *
 *  Created on: Aug 30, 2019
 *      Author: user
 */

#include <iostream>

#include <cmath>
using namespace std;

void swap(int*, int*);

int mainTFM(){
	int num1 = 55, num2 = 66;

	cout<<"Before swap Num1=" << num1 << "Num2=" << num2
			<< endl;
	swap(&num1, &num2);
	cout<<"After swap Num1=" << num1 << "Num2=" << num2
				<< endl;
	return 0;

}

void swap(int *x, int *y){
	int temp = *x;
	*x = *y;
	*y = temp;
}
