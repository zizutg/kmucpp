#ifndef AUXIL_H
#define AUXIL_H

// Aux class declaration.
class Aux
{
private:
   double auxBudget;
public:
   Aux() { auxBudget = 0; }
   void addAuxget(double);
   double getAuxBudget() { return auxBudget; }
};
#endif
