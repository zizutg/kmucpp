// This program demonstrates the action of 
// convert constructors.

#include <iostream>
#include <string>

using namespace std;

class IntClass
{
private:
  int value;
public:
  // Convert constructor from int.
  IntClass(int intValue)
  {
    value = intValue;
  }

  int getValue(){ return value; }
};


//*******************************************
// This function returns an int even though *
// an IntClass object is declared as the    *
// return type.                             *
//*******************************************
IntClass f(int intValue)
 {
     return intValue;
 }

//*******************************************
// Prints the int value inside an IntClass  *
// object.                                  *
//*******************************************
void printValue(IntClass x)
{
   cout << x.getValue();
}

int mainCTD()
{
   // Initialize with an int.
   IntClass intObject(23);
   cout << "The value is " << intObject.getValue() << endl;

   // Assign an int.
   int x = 24;
   intObject = x;
   cout  << "The value is " << intObject.getValue()  << endl;

   // Pass an int to a function expecting IntClass.
   cout << "The value is "; 
   printValue(25);
   cout << endl;

   // Demonstrate conversion on a return.
   intObject = f(26);
   cout << "The value is ";
   printValue(intObject);

   return 0; 
}

