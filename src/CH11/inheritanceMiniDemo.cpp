#include <iostream>
using namespace std;

class Person {
private:
	string profession;
	int age;
public:

	Person() :
			profession("unemployed"), age(16) {
	}
	void display() {
		cout << "My profession is: " << profession << endl;
		cout << "My age is: " << age << endl;
		cout << "I can walk." << endl;
		cout << "I can talk." << endl;
	}
	void setProfession(string p) {
		profession = p;
	}
	void setAge(int a) {
		age = a;
	}

	void teachs() {
			cout << "Nothing" << endl;
	}
};
// MathsTeacher class is derived from base class Person.
class MathsTeacher: public Person {
public:
	void teachs() {
		cout << "I can teach Maths." << endl;
	}
};
// Footballer class is derived from base class Person.
class Footballer: public Person {
public:
	void teachs() {
		cout << "I can teach, how to play Football." << endl;
	}
};
int mainIMD() {

	MathsTeacher teacher;
	teacher.setProfession("Teacher");
	teacher.setAge(23);
	teacher.display();
	teacher.teachs();

	cout << endl;

	Footballer footballer;
	footballer.setProfession("Footballer");
	teacher.setAge(49);
	footballer.display();
	footballer.teachs();

	return 0;
}
