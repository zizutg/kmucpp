// Used by ThisExample.cpp
#ifndef EXAMPLE_H
#define EXAMPLE_H
class Example
{
 private:
   int x;
 public:
   Example(int a){ x = a;}
   void setValue(int);
   void printAddressAndValue();
};
#endif
