// This program illustrates how to copy one array to another.
#include <iostream>
using namespace std;

void displayArray(int [], int);
void copyArray(int [], int [], int);

int mainAC()
{
	const int SIZE = 6;
	int arrayA[SIZE] = {10, 20, 30, 40, 50, 60};
	int arrayB[SIZE] = { 2,  4,  6,  8, 10, 12};
	

	copyArray(arrayA, arrayB, SIZE);
	// Display the contents of the two arrays
	cout << "ArrayB still holds the values:    ";
	displayArray(arrayB, SIZE);

	cout << "\nArrayA now also holds the values: ";
	displayArray(arrayA, SIZE);
	cout << endl;
	return 0;
}

void copyArray(int a1[], int a2[], int size) {
	// Copy the elements of a2 to a1
	for (int index = 0; index < size; index++)
		a1[index] = a2[index];
}


void displayArray(int array[], int size) {

	for (int index = 0; index < size; index++)
		cout << array[index] << "  ";

}
